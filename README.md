# Welcome to guide Socket for UANI

“UNAI” เทคโนโลยีระบุตำแหน่งภายในอาคาร เสริมแกร่งธุรกิจจัดงานอีเวนต์
![Sci Update_UNAI_MICE](https://www.nstda.or.th/home/wp-content/uploads/2022/01/Banner_UNAI-01.jpg)

## Document

|                            | Document                                                                                                    |
| -------------------------- | ----------------------------------------------------------------------------------------------------------- |
| Socket @ realtime location | https://docs.google.com/document/d/1tfJJL8-qdNy4muN8gbIAV2-7pLm-0Jy3Q71fXpU-4Ls/edit#heading=h.pwdi2lxhqjp6 |
| Socket @ Racing            | https://docs.google.com/document/d/1tfJJL8-qdNy4muN8gbIAV2-7pLm-0Jy3Q71fXpU-4Ls/edit#heading=h.i7mi0dta9nbt |

## UML diagrams example, for realtime location diagram:

```mermaid


sequenceDiagram
Client ->> UNAI[API]: gen_token
UNAI[API] -->> Client: access_token
Client ->> UNAI[API]: gen_encrypt_topic
UNAI[API] -->> Client: encrypt_topic,socket_token
Client ->> UNAI[Socket]: connecting... socket
UNAI[Socket] -->> Client : connected
Note  left of  Client: [SOCKET_ID]

Client ->> UNAI[Socket]: Join topic : init_unai_location_tag
Client ->> UNAI[Socket]: Join topic : init_unai_location_anchor
Client ->> UNAI[Socket]: broadcastToRoom topic : init_unai_location
Note  left of  Client:  { action:  'get_init_unai_location', <br/>customId:  [CUSTOM_ID],<br/> socketGetInitId:  [SOCKET_ID],<br/> getMode:  'only', <br/>get_topic:  encrypt_topic, <br/>get_floor:  [FLOOR_ID] }

UNAI[Socket] -->> Client : init_unai_location_tag
UNAI[Socket] -->> Client : init_unai_location_anchor

Client ->> UNAI[Socket]: Join topic : init_unai_location_tag_received
Note  left of  Client:  { action:  'init_unai_location_tag_received', <br/>customId:  [CUSTOM_ID],<br/> socketGetInitId:  [SOCKET_ID],<br/> getMode:  'only', <br/>get_topic:  encrypt_topic, <br/>get_floor:  [FLOOR_ID] }
Client ->> UNAI[Socket]: Join topic : init_unai_location_anchor_received
Note  left of  Client:  { action:  'init_unai_location_anchor_received', <br/>customId:  [CUSTOM_ID],<br/> socketGetInitId:  [SOCKET_ID],<br/> getMode:  'only', <br/>get_topic:  encrypt_topic, <br/>get_floor:  [FLOOR_ID] }

Client ->> UNAI[Socket]: Join topic : unai/[encrypt_topic]/tag
Client ->> UNAI[Socket]: Join topic : unai/[encrypt_topic]/anchor

UNAI[Socket] -->> Client : clientBox
Note  right of  UNAI[Socket]:  { [DATA_TAG] }
UNAI[Socket] -->> Client : clientBox
Note  right of  UNAI[Socket]:  { [DATA_ANCHOR] }

Client ->> Display : Real Time

```

## UML diagrams example, for racing diagram:

```mermaid


sequenceDiagram
Client ->> UNAI[API]: gen_token
UNAI[API] -->> Client: access_token
Client ->> UNAI[API]: gen_encrypt_topic
UNAI[API] -->> Client: encrypt_topic,socket_token
Client ->> UNAI[Socket]: connecting... socket
UNAI[Socket] -->> Client : connected



Client ->> UNAI[Socket]: Join topic : unai/[encrypt_topic]/race
Client -->> UNAI[Socket]: Join topic : unai/[encrypt_topic]/event/count

UNAI[Socket] -->> Client : clientBox
Note  right of  UNAI[Socket]:  { [DATA_TAG] }

Client ->> Display : Real Time

```
