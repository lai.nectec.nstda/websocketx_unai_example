//import module
var socketClientModule = require('./_client.js');

let options = {};
options.query = {
	/* This token Exprie 30/09/2023 */
	token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsaWNlbnNlS2V5IjoibGljZW5zZUtleSIsImlhdCI6MTY5MzM3ODg1OCwiZXhwIjoxNjk1OTcwODU4fQ.vEYCQL7YwofJ2qo8PLhjY4Myfwe3bRoKYmPIF0LxU-A',
};
let socket = socketClientModule.setup(
	'https://socketx.lailab.online',
	'/ble/location',
	'/',
	(socket) => {
		console.log('socket conected');
		socketClientModule.register('user' + '_' + Math.floor(1000 + Math.random() * 9000), socket);

		// For get data lap //
		// [encrypt_topic] : https://docs.google.com/document/d/1tfJJL8-qdNy4muN8gbIAV2-7pLm-0Jy3Q71fXpU-4Ls/edit#heading=h.qehtwwavarfz
		// socketClientModule.join('unai/[encrypt_topic]/event/count', socket);
		// socketClientModule.join('unai/[encrypt_topic]/race', socket);

		// For reset lap //
		// socketClientModule.broadcastToRoom('unai_location/event', { action: 'unai_location/event', eventname: '[EVENTNAME]', mode: 'reset' }, socket);

		// For reset_tag //
		// socketClientModule.broadcastToRoom('unai_location/event', { action: 'unai_location/event', eventname: '[EVENTNAME]', mode: 'reset_tag', tag_id: '[TAG_ID]' }, socket);
	},
	options
);

socket.on('clientBox', function (data) {
	//console.log('['+socket.id+']: ' + data);
	console.dir(data, { depth: null, colors: true });
	//console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
});
