//import module
var socketClientModule = require('./_client.js');

var request = require('request-promise');
var options = {
	method: 'POST',
	url: 'https://rtls.lailab.online/auth/gen_token',
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded',
	},
	form: {
		username: 'unaiDemo', //--- change to your username ---//
		password: 'demo@Unai', //--- change to your password ---//
	},
};

var socketx, socket_on_connected, access_token, encrypt_topic, socket_token;

request(options)
	.then(function (res) {
		// console.log('response', res);
		let response = JSON.parse(res);
		console.log(response);
		access_token = response.access_token;

		var options = {
			method: 'POST',
			url: 'https://rtls.lailab.online/gen_encrypt_topic',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				Authorization: `Bearer ${access_token}`,
			},
			form: {
				floorID: '8', //--- change to your floorID ---//
			},
		};

		return request(options);
	})
	.then(function (res) {
		// console.log('response', res);
		let response = JSON.parse(res);
		console.log(response);
		encrypt_topic = response.encrypt_topic;
		socket_token = response.socket_token;

		socket_connect();
	})
	.catch(function (err) {
		// API call failed...
		console.log(err);
	});

function socket_connect() {
	let idx;
	var options = {};
	options.query = {
		/* This token Exprie 30/09/2023 */
		token: socket_token,
	};
	console.log('socket conecting...');
	socketx = socketClientModule.setup(
		'https://socketx.lailab.online',
		'/ble/location',
		'/',
		(socket) => {
			console.log('socket conected');
			socket_on_connected = socket;
			idx = 'user' + '_' + Math.floor(1000 + Math.random() * 9000) + '_' + Math.floor(1000 + Math.random() * 9000);
			socketClientModule.register(idx, socket);

			socketClientModule.join('init_unai_location_tag', socket_on_connected);
			socketClientModule.join('init_unai_location_anchor', socket_on_connected);
			console.log('socket.id : ', socket.id);
			console.log('SENT init_unai_location ==>');
			socketClientModule.broadcastToRoom(
				'init_unai_location',
				{ action: 'get_init_unai_location', customId: idx, socketGetInitId: socket.id, getMode: 'only', get_topic: encrypt_topic, get_floor: '5' },
				socket_on_connected
			);
		},
		options
	);

	// socketx.on('connect', function () {
	// 	console.log('connection open :)');
	// });

	// หากต้องการข้อมูลทั่วไปให้ใช้ Events ชื่อ clientBox
	socketx.on('clientBox', function (data) {
		if (typeof data === 'object' && data !== null) {
			if (data.topic != undefined || data.topic != 'NULL') {
				console.log('[clientBox data] :', data.topic);
				if (data?.topic?.includes('/tag')) {
					console.log('[clientBox /tag] :', data);
					// calculateCumulatedTag(data);
				} else if (data?.topic?.includes('/anchor')) {
					console.log('[clientBox /anchor] :', data);
					// calculateCumulatedAnchor(data);
				}
			}
		} else {
			console.log('[clientBox] :', data);
		}
	});

	// แต่ถ้าเป็นข้อมูล sensor ให้ใช้ Events ชื่อ sensor
	socketx.on('sensor', function (data) {
		console.log('[sensor] :', data);
	});

	socketx.on('init_unai_location_anchor', function (message) {
		// console.log('[init_unai_location_anchor] :', data);
		console.log('init_unai_location_anchor_received', 'OK');
		is_get_init_unai_location_anchor = true;
		socketClientModule.broadcastToRoom('init_unai_location', { action: 'init_unai_location_anchor_received', customId: idx, socketGetInitId: socket_on_connected.id }, socket_on_connected);
		anchor_topic = `unai/${encrypt_topic}/anchor`;
		socketClientModule.join(anchor_topic, socket_on_connected);

		// for(const[key,value] of Object.entries(message)){
		//   calculateCumulatedAnchor(value);
		// }
	});

	socketx.on('init_unai_location_tag', (message) => {
		// socket.emit('init_unai_location_tag_received','OK');
		console.log('init_unai_location_tag_received', 'OK');
		is_get_init_unai_location_tag = true;
		socketClientModule.broadcastToRoom('init_unai_location', { action: 'init_unai_location_tag_received', customId: idx, socketGetInitId: socket_on_connected.id }, socket_on_connected);
		tag_topic = `unai/${encrypt_topic}/tag`;
		socketClientModule.join(tag_topic, socket_on_connected);

		//..........
	});
}
