//import module
var socketClientModule = require('./_client.js');

var request = require('request-promise');
var options = {
	method: 'POST',
	url: 'https://rtls.lailab.online/auth/gen_token',
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded',
	},
	form: {
		username: 'unaiDemo', // change to your username //
		password: 'demo@Unai', // change to your password //
	},
};

var socketx, socket_on_connected, access_token, encrypt_topic, socket_token;

request(options)
	.then(function (res) {
		// console.log('response', res);
		let response = JSON.parse(res);
		console.log(response);
		access_token = response.access_token;

		var options = {
			method: 'POST',
			url: 'https://rtls.lailab.online/gen_encrypt_topic',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				Authorization: `Bearer ${access_token}`,
			},
			form: {
				floorID: '8', // change to your floorID //
			},
		};

		return request(options);
	})
	.then(function (res) {
		// console.log('response', res);
		let response = JSON.parse(res);
		console.log(response);
		encrypt_topic = response.encrypt_topic;
		socket_token = response.socket_token;

		socket_connect();
	})
	.catch(function (err) {
		// API call failed...
		console.log(err);
	});

function socket_connect() {
	let idx;
	var options = {};
	options.query = {
		/* This token Exprie 30/09/2023 */
		token: socket_token,
	};
	console.log('socket conecting...');
	socketx = socketClientModule.setup(
		'https://socketx.lailab.online',
		'/ble/location',
		'/',
		(socket) => {
			console.log('socket conected');
			socket_on_connected = socket;
			idx = 'user' + '_' + Math.floor(1000 + Math.random() * 9000) + '_' + Math.floor(1000 + Math.random() * 9000);
			socketClientModule.register(idx, socket);

			let forkloft_topic = `unai/${encrypt_topic}/forkloft`;
			socketClientModule.join(forkloft_topic, socket_on_connected);
		},
		options
	);

	// socketx.on('connect', function () {
	// 	console.log('connection open :)');
	// });

	// หากต้องการข้อมูลทั่วไปให้ใช้ Events ชื่อ clientBox
	socketx.on('clientBox', function (data) {
		if (typeof data === 'object' && data !== null) {
			if (data.topic != undefined || data.topic != 'NULL') {
				console.log('[clientBox data] :', data.topic);
				if (data?.topic?.includes('/forkloft')) {
					console.log('[clientBox /forkloft] :', data);
					// calculateCumulatedTag(data);
				}
			}
		} else {
			console.log('[clientBox] :', data);
		}
	});
}
